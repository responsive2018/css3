# CSS 3

#### En esta sección se presentarán elementos de CSS junto con elementos para poder hacer responsive design


## Temario


* Que es WRD (Web Responsive Design)
* Media Queries
* Includes
* View Port
* Resolución de pantalla
* Porcentajes
* Background
* Alineaciones y positions
* Transitions


==========

## Links de interes
[CSS Tricks](https://css-tricks.com/)

## Web Responsive WRD

Responsive Web Design ayuda para poder hacer ver bien un sitio web en todos los dispositivos (desktops, tablets y teléfonos).
Responsive Web Design utiliza HTML y CSS para escalar, ocultar, mostrar omover contenido para garantizar que siempre se vea bien en cualquier dispositivo.


![alt text](http://responsivedesignchecker.com/images/devices.png "Logo ejemplo")


## Media Queries

Las media queries nos sirven para poder manejar un css distinto para una resolución de pantalla especifica, por ejemplo:

//en resoluciones mayores a 600 px

body {
  background-color: #000;
  color: #fff;
}

  @media only screen and (max-width: 600px)
  {

    body {
        background-color: lightblue;
        color: RED;

  }

Es importante resaltar que para este tema podemos jugar con google chrome y su herramienta de desarrolladores.


## includes

Este srive para poder incluir otro css que estemos trabajando dentro del principal, así podemos ser mas modulares

@import "printstyle.css";




## View Port

Este es un meta de html que nos sirve para indicar que cuando estemos en un dispositivo movil, la informacion se vea clara y consisa al tamaño y resolucion de nuestro dispositivo

`<meta name="viewport" content="width=device-width, initial-scale=1.0">`


## Porcentajes

Para el tema de responsive, los porcentajes son sumamente importantes, debido a que por ellos podemos hacer que los elementos de un HTML se vean responsive y ajustables según la resolución de pantalla, por ejemplo una imagen o un div que contenga información:

Ejemplo en vivo.


## Background

El Background, en mobile es muy comun que se utilice con un elemento llamado size cover, como por ejemplo:

html {

  background: url(images/bg.jpg) no-repeat center center fixed;

  -webkit-background-size: cover;

  -moz-background-size: cover;

  -o-background-size: cover;

  background-size: cover;

}


## Alineaciones y positions

Para el caso de las alineaciones de elementos, veremos que en muchas ocaciones es necesarios alinear un elemento con respecto a otro, donde veremos como se logra con position y los floats.


## Transitions

Los transitions nos ayudan a dar un poco de estilo a los elementos que tienen hover y que puden cambiar de color o que cambien de tipo de letra, etc.
